# THIS REPO IS DEPRECATED
There's a better way to get tiling functionality to KDE by simply switching kwin to bspwm. Khrönkite script is buggy and unreliable.

# KDE dwm config
![screenshot of kde](https://gitlab.com/fawzakin/kdwm/raw/main/kdwm.png)  
This repo is made for my personal configuration and theme for KDE Plasma that has been modified to look and behave like dwm.

# Applets
- Window Title
- Launchpad Plasma
- [virtual-desktop-bar](https://github.com/wsdfhjxc/virtual-desktop-bar)
- [do-it-yourself-bar](https://github.com/wsdfhjxc/do-it-yourself-bar)
 
# Instruction
WIP. I need to get any nessecary config from my KDE desktop and test my instruction to other Linux system running vanilla KDE.
NOTE: "C/S" means "Copy or Stow". You can copy the files manually or use stow.
 
1. (Optional) C/S the theme folder if you want to use red solus theme.
```
cp -rf kde-theme/* ~
```
2. Install [Kröhnkite](https://github.com/esjeon/krohnkite) from system setting or from the repo.
3. C/S Kröhnkite config
```
cp -rf khronkite-conf/* ~
```
4. C/S kde-conf. Make sure you cd into this repo first.
```
cp -rf kde-conf/* ~
```
5. Install the previously mentioned applets. For the bar applets, you need to manually compile them yourself.
TIPS: Run `bar-install.sh` as root to quickly install dependencies (supports Arch,Debian/Ubuntu, Fedora, Solus, and OpenSUSE) and build the bar applets
6. Configure the panel like the screenshot. For the DIY script, the path is /home/yourname/.config/kdebar (you can NOT use ~ or $HOME) More detailed steps coming soon.

# License and Credit
MIT License. Credit to wsdfhjxc for the great KDE widgets made for TWM users.
