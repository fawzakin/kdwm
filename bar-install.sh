#!/bin/sh
[ "$(id -u)" -ne 0 ] && echo "Please run as root." && exit 1

name="$(logname)"
name_home="$(sudo -u ${name} -H -s eval 'echo $HOME')"
src="${name_home}/.src" && sudo -u ${name} mkdir ${src}

[ -x "$(command -v pacman)" ] &&
    pacman -S --needed cmake extra-cmake-modules gcc make git
[ -x "$(command -v apt)" ] &&
    apt install cmake extra-cmake-modules g++ qtbase5-dev qtdeclarative5-dev libqt5x11extras5-dev libkf5plasma-dev libkf5globalaccel-dev libkf5xmlgui-dev git
[ -x "$(command -v dnf)" ] &&
    dnf install cmake extra-cmake-modules gcc-c++ qt5-qtbase-devel qt5-qtdeclarative-devel qt5-qtx11extras-devel kf5-plasma-devel kf5-kglobalaccel-devel kf5-kxmlgui-devel git
[ -x "$(command -v eopkg)" ] &&
    eopkg install cmake extra-cmake-modules qt5-base-devel qt5-declarative-devel qt5-x11extras-devel plasma-framework-devel kglobalaccel-devel kxmlgui-devel git
[ -x "$(command -v zypper)" ] &&
    zypper in cmake extra-cmake-modules gcc-c++ libqt5-qtbase-devel libqt5-qtdeclarative-devel libqt5-qtx11extras-devel plasma-framework-devel kglobalaccel-devel kxmlgui-devel git

sudo -u ${name} git clone https://github.com/wsdfhjxc/virtual-desktop-bar ${src}/vdb
sudo -u ${name} mkdir -p ${src}/vdb/build
cd ${src}/vdb/build
sudo -u ${name} cmake ..
make install

sudo -u ${name} git clone https://github.com/wsdfhjxc/do-it-yourself-bar ${src}/diyb
sudo -u ${name} mkdir -p ${src}/diyb/build
cd ${src}/diyb/build
sudo -u ${name} cmake ..
make install

# Fixing Applets if KDE didn't find it
qt_r_dir=/usr/lib/qt5/qml/org/kde/plasma
qt_w_dir=/usr/lib/qt/qml/org/kde/plasma
[ ! -d "${qt_r_dir}/virtualdesktopbar" ] && cp -rf "${qt_w_dir}/virtualdesktopbar" "${qt_r_dir}"
[ ! -d "${qt_r_dir}/doityourselfbar" ] && cp -rf "${qt_w_dir}/doityourselfbar" "${qt_r_dir}"
